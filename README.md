# Converting audio file from wav format to sph (NIST) format

The code converts the audio file from wav format to sph format and adds the relevant headers to the sph file for easy access of metadata. Each sph file will have a header with the relevant information of the specific audio file along with the audio file.


## Requirement

Install **sox** command for this code to work

```bash
  sudo apt-get install sox
```
    
## Header information

Edit header information in ```Line 22-27``` of ```wav2sph.sh``` before running the code.

`database_id='IITM_TTS'`

`database_ver='TTS_Phase3'`

`utterance_id='utt_id'`

`language_id='Hindi'`

`speaker_id='SpeakerA'`

`gender_id='male'`

Change ```printflag``` in ```Line 38``` of ```wav2sph.sh``` to ```0``` to not print anything on the screen.

## Usage
For converting the audio file from wav format to sph format.

Usage - version 1
```
bash wav2sph.sh
```
```
Usage: bash wav2sph.sh <input-wav-file> [<output-sph-file>] [<utterance-id>]
                       [<output-sph-file>] and [<utterance-id>] are optional
                                    Command line arguments are read in order
```
Usage - version 2
```
bash wav2sph_v2.sh
```
```
Usage: bash wav2sph_v2.sh <input-wav-file> [<output-sph-file>] [<language-id>] [<gender-id>] [<speaker-id>] [<utterance-id>]
                             Except <input-wav-file> all other arguments are optional
                                    Command line arguments are read in order
```
Usage - version 3
```
bash wav2sph_v3.sh
```
```
Usage: bash wav2sph_v3.sh <input-wav-file> [<output-sph-file>] [<spoken-text>] [<language-id>] [<gender-id>] [<speaker-id>] [<utterance-id>]
                             Except <input-wav-file> all other arguments are optional
                                    Command line arguments are read in order
```

#### Example 1
Run
```bash
bash wav2sph.sh path/audio.wav
```
Output
```bash
Input: path/audio.wav
Output: path/audio.sph
Utterance_id: audio
Converting wav to sph
Adding headers to sph files
Changing sample_count
Done
```
#### Example 2
Run
```bash
bash wav2sph.sh path/audio.wav output_folder/output.sph
```
Output
```bash
Input: path/audio.wav
Output: output_folder/output.sph
Utterance_id: audio
Converting wav to sph
Adding headers to sph files
Changing sample_count
Done
```
#### Example 3
Run
```bash
bash wav2sph.sh path/audio.wav output_folder/output.sph utt_id
```
Output
```bash
Input: path/audio.wav
Output: output_folder/output.sph
Utterance_id: utt_id
Converting wav to sph
Adding headers to sph files
Changing sample_count
Done
```
#### Example 4
Parallelly run for many wav files.
```bash
less list_of_wav_files.txt | parallel "bash wav2sph.sh {1} output_folder/{1/.}.sph"
```
#### Example 5
Pass the other header informations as command line arguments. This option is only available for the next version of the wav2sph code.
```bash
bash wav2sph_v2.sh path/audio.wav output_folder/output.sph Hindi male speaker1 utt_id
```
#### Example 6
Pass label information also along with the other headers.
```bash
bash wav2sph_v3.sh path/audio.wav output_folder/output.sph label_in_Hindi Hindi male speaker1 utt_id
```

## Using sph file

#### Reading the header information
The header information can be read just like reading reading a file.

Run
```bash
vim audio.sph
```
Output
```bash
NIST_1A
   1024
database_id -s8 IITM_TTS
database_ver -s10 TTS_Phase3
utterance_id -s23 audio
language_id -s5 Hindi
speaker_id -s8 SpeakerA
gender_id -s4 male
sample_count -i 17903303
sample_n_bytes -i 2
channel_count -i 1
sample_byte_format -s2 01
sample_rate -i 44100
sample_coding -s3 pcm
end_head
```

- ```-s8```: sting of length 8
- ```-i```: integer
- ```NIST_1A 1024```: NIST header, first 1024 bytes are header information

Header with the text
```bash
NIST_1A
   1024
database_id -s8 IITM_TTS
database_ver -s11 TTS_Phase-3
utterance_id -s15 story_0001-0001
text -s27 बोले हुए शब्द वापस नहीं आते
language_id -s5 Hindi
gender_id -s4 male
speaker_id -s8 SpeakerA
sample_count -i 123809
sample_n_bytes -i 2
channel_count -i 1
sample_byte_format -s2 01
sample_rate -i 48000
sample_coding -s3 pcm
end_head
```

#### Converting sph file to wav file
Run
```bash
sox -t sph audio.sph -t wav audio.wav
```

#### Sox can read infomation of sph file
Run
```bash
sox --info audio.sph
```
Output
```bash
Input File     : 'audio.sph'
Channels       : 1
Sample Rate    : 44100
Precision      : 16-bit
Duration       : 00:06:45.97 = 17903303 samples = 30447.8 CDDA sectors
File Size      : 35.8M
Bit Rate       : 706k
Sample Encoding: 16-bit Signed Integer PCM
```

#### Using sph file in Matlab and Python

Matlab - Use [Voicebox toolkit](http://www.ee.ic.ac.uk/hp/staff/dmb/voicebox/mdoc/v_mfiles/v_readsph.html) to read sph file in Matlab.

Python - Use [librosa toolkit](https://librosa.org/doc/latest/generated/librosa.load.html#) to read sph file in Python.

## Author(s)

- [Jom Kuriakose](https://gitlab.com/Jolverine)

  
