#!/bin/bash
# Script to convert the wav audio file to NIST format (SPH file)

# Check the command line arguments
if ! [[ $# -ge 1 && $# -le 3 ]];
then
echo "Usage: bash wav2sph.sh <input-wav-file> [<output-sph-file>] [<utterance-id>]"
echo "                       [<output-sph-file>] and [<utterance-id>] are optional"
echo "                                    Command line arguments are read in order"
exit 0
fi

# Read wav file as argument
wavfile=$1
wavname=`basename $wavfile .wav`
wavpath=`dirname $wavfile`
sphfile=${2:-$wavpath\/$wavname.sph}
tmpfile=$wavpath\/$wavname.tmp
logfile=$wavpath\/$wavname.log

# Header Information
database_id='IITM_TTS'
database_ver='TTS_Phase-3'
utterance_id=${3:-$wavname}
language_id='Hindi'
speaker_id='Speaker1'
gender_id='male'

# Header Format
database_id_head="database_id -s${#database_id} $database_id"
database_ver_head="database_ver -s${#database_ver} $database_ver"
utterance_id_head="utterance_id -s${#utterance_id} $utterance_id"
language_id_head="language_id -s${#language_id} $language_id"
speaker_id_head="speaker_id -s${#speaker_id} $speaker_id"
gender_id_head="gender_id -s${#gender_id} $gender_id"

# 0 - No printing on screen, 1 - Printing on screen
printflag=0

if [[ $printflag -ne 0 ]]
then
echo "Input: "$wavfile
echo "Output: "$sphfile
echo "Utterance_id: "$utterance_id
echo "Converting wav to sph"
fi

# Convert wav to sph file
sox -t wav $wavfile -t sph $sphfile

if [[ $printflag -ne 0 ]]
then
echo "Adding headers to sph files"
fi

# Adding the header information to sph file
sed -i "3 i $gender_id_head" $sphfile
sed -i "3 i $speaker_id_head" $sphfile
sed -i "3 i $language_id_head" $sphfile
sed -i "3 i $utterance_id_head" $sphfile
sed -i "3 i $database_ver_head" $sphfile
sed -i "3 i $database_id_head" $sphfile

if [[ $printflag -ne 0 ]]
then
echo "Changing sample_count"
fi

# Change the sample count to match the sph file with header
sox --info $sphfile 2>$tmpfile 1>$logfile # Reading the sample count using sox command
sample_count=`awk 'NF>1{print $NF}' $tmpfile`
sample_count_head="sample_count -i $sample_count"

# Adding sample count header
sed -i '9d' $sphfile
sed -i "9 i $sample_count_head" $sphfile

# Removing the temporary files
rm $tmpfile
rm $logfile

if [[ $printflag -ne 0 ]]
then
echo "Done"
fi

# Done
